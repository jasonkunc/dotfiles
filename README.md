# .dotfiles

It may not be pretty but, it's mine. 

### Mac install
- homebrew
- iTerm
- neovim
- vim plugins
- zsh
- airline
- theme(s)

### Xubuntu install
- apt-get
- Hyper Terminal / Termite
- neovim
- vim plugins
- zsh
- airline
- theme(s)